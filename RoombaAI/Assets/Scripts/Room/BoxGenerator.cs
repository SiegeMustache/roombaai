﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxGenerator : MonoBehaviour
{
    public int planeXSize;
    public int planeYSize;
    public GameObject boxUnit;

    // Start is called before the first frame update
    void Start()
    {
        for(int i = 0; i < planeXSize; i++)
        {
            for(int j = 0; j < planeYSize; j++)
            {
                GameObject go = Instantiate(boxUnit, new Vector3(i, 0, j), Quaternion.identity);
            }
        }
    }
}
