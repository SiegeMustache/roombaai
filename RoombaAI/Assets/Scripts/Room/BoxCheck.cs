﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCheck : MonoBehaviour
{
    MeshRenderer mesh;
    public Material cleanMaterial;
    public bool isCleaned = false;
    public LayerMask freeBox;
    public LayerMask occupiedBox;

    // Start is called before the first frame update
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
    }

    public void CleanBox()
    {
        mesh.material = cleanMaterial;
        isCleaned = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Roomba"))
        {
            this.gameObject.layer = LayerMask.NameToLayer("OccupiedBox");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Roomba"))
        {
            this.gameObject.layer = LayerMask.NameToLayer("Box");
        }
    }
}
