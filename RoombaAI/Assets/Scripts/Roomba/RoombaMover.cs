﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoombaMover : MonoBehaviour
{
    #region PUBLIC VARIABLES
    public float moveTime;
    #endregion

    #region COMPONENTS
    Transform thisTransform;
    RoombaCleaner cleaner;
    RoombaAI roombaAI;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        thisTransform = GetComponent<Transform>();
        cleaner = GetComponent<RoombaCleaner>();
        roombaAI = GetComponent<RoombaAI>();
    }

    public IEnumerator MoveToBox(Vector3 startPos, GameObject targetPos, float overTime)
    {
        Vector3 originalStartPos = startPos;

        thisTransform.LookAt(new Vector3(targetPos.transform.position.x, thisTransform.transform.position.y, targetPos.transform.position.z));

        float startTime = Time.time;
        while (Time.time < startTime + overTime)
        {
            thisTransform.position = Vector3.Lerp(originalStartPos, targetPos.transform.position, (Time.time - startTime) / overTime);
            yield return null;
        }

        if(Vector3.Distance(thisTransform.position, targetPos.transform.position) < 0.1f)
        {
            cleaner.CleanBox(targetPos);
            roombaAI.ResetStatus();
        }
    }

    public IEnumerator RotateTowardsBox(GameObject box)
    {
        thisTransform.LookAt(new Vector3(box.transform.position.x, thisTransform.transform.position.y, box.transform.position.z));
        yield return new WaitForSeconds(1);
        roombaAI.ResetStatus();
    }
}
