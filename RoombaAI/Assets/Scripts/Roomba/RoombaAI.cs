﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoombaAI : MonoBehaviour
{
    #region PUBLIC VARIABLES
    public enum TypeOfAI { FirstVersion, SecondVersion}
    public TypeOfAI currentTypeOfAI;

    public LayerMask boxLayer;
    public LayerMask wallLayer;
    #endregion

    #region PRIVATE VARIABLES
    GameObject m_rightWall;
    GameObject m_forwardWall;
    GameObject m_leftWall;

    GameObject m_currentTarget;
    #endregion

    #region COMPONENTS
    RoombaMover mover;
    Transform thisTransform;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        mover = GetComponent<RoombaMover>();
        thisTransform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        switch(currentTypeOfAI)
        {
            case TypeOfAI.FirstVersion:
                {
                    if(!m_currentTarget)
                        FirstVersionAI();
                    break;
                }
            case TypeOfAI.SecondVersion:
                {
                    if(!m_currentTarget)
                    {
                        SecondVersionAI();
                    }
                    break;
                }
        }
    }

    #region UTILITY METHOD
    public void ResetStatus()
    {
        m_rightWall = null;
        m_forwardWall = null;
        m_leftWall = null;
        m_currentTarget = null;
    }
    #endregion

    #region SENSORS AND RELATED
    void CheckSurroundings(LayerMask layer)
    {
        m_rightWall = CheckDirection(thisTransform.right, layer);
        m_forwardWall = CheckDirection(thisTransform.forward, layer);
        m_leftWall = CheckDirection(-thisTransform.right, layer);
    }

    GameObject CheckDirection(Vector3 direction, LayerMask layer)
    {
        if (Physics.Raycast(thisTransform.position, direction, out RaycastHit hit, 0.55f, layer))
            return hit.collider.gameObject;
        else
            return null;
    }

    bool CheckForBoxStatus(GameObject box)
    {
        //given box, check if it's cleaned
        bool value = box.GetComponent<BoxCheck>().isCleaned;
        return value;
    }
    #endregion

    #region FIRST VERSION AI
    void FirstVersionAI()
    {
        //check for walls nearby
        CheckSurroundings(wallLayer);

        if (!m_rightWall && !m_leftWall && !m_forwardWall)
        {
            //no walls around, so lets see if there is any dirt around

            GameObject[] nearbyBoxes = new GameObject[3];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(thisTransform.forward, boxLayer);
            nearbyBoxes[2] = CheckDirection(-thisTransform.right, boxLayer);

            //if all boxes are clean, go to a random box
            if(CheckForBoxStatus(nearbyBoxes[0]) && CheckForBoxStatus(nearbyBoxes[1]) && CheckForBoxStatus(nearbyBoxes[2]))
                StartMovement(nearbyBoxes[UnityEngine.Random.Range(0,3)]);

            //if the box on the right is dirty, go clean
            else if(!CheckForBoxStatus(nearbyBoxes[0]))
                StartMovement(nearbyBoxes[0]);

            //if the box ahead is dirty, go clean
            else if (!CheckForBoxStatus(nearbyBoxes[1]))
                StartMovement(nearbyBoxes[1]);
            
            //if the box on the left is dirty, go clean
            else if (!CheckForBoxStatus(nearbyBoxes[2]))
                StartMovement(nearbyBoxes[2]);
        }
        else if (m_rightWall && m_forwardWall && m_leftWall)
        {
            //there are walls on all the sensors sides, lets rotate to the right
            StartRotation(m_rightWall);
        }
        else if (m_rightWall)
        {
            //there is a wall on the right side
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.forward, boxLayer);
            nearbyBoxes[1] = CheckDirection(-thisTransform.right, boxLayer);

            if(nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if both boxes are clean, go to the one ahead
                if (CheckForBoxStatus(nearbyBoxes[0]) && CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[0]);

                //if the box ahead is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the box on the left is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check which one of the boxes is missing, and in case, move to the other existing
                if (!nearbyBoxes[0] && nearbyBoxes[1])
                    StartMovement(nearbyBoxes[1]);

                else if (nearbyBoxes[0] && !nearbyBoxes[1])
                    StartMovement(nearbyBoxes[0]);
            }
        }
        else if (m_forwardWall)
        {
            //there is a wall ahead
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(-thisTransform.right, boxLayer);

            if(nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if both boxes are clean, go to the right one
                if (CheckForBoxStatus(nearbyBoxes[0]) && CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[0]);

                //if the right box is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the left box is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check which one of the boxes is missing, and in case, move to the other existing
                if (!nearbyBoxes[0] && nearbyBoxes[1])
                    StartMovement(nearbyBoxes[1]);

                else if (nearbyBoxes[0] && !nearbyBoxes[1])
                    StartMovement(nearbyBoxes[0]);
            }
        }
        else if (m_leftWall)
        {
            //there is a wall on the left
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(thisTransform.forward, boxLayer);

            if(nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if both boxes are clean, go to the one ahead
                if (CheckForBoxStatus(nearbyBoxes[0]) && CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);

                //if the right box is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the box ahead is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check if one of the boxes is missing, and in case, move to the other existing
                if (!nearbyBoxes[0] && nearbyBoxes[1])
                    StartMovement(nearbyBoxes[1]);

                else if (nearbyBoxes[0] && !nearbyBoxes[1])
                    StartMovement(nearbyBoxes[0]);
            }
        }
    }
    #endregion

    #region SECOND VERSION AI
    void SecondVersionAI()
    {
        //check for walls nearby
        CheckSurroundings(wallLayer);

        if (!m_rightWall && !m_leftWall && !m_forwardWall)
        {
            //no walls around, so lets see if there is any dirt around

            GameObject[] nearbyBoxes = new GameObject[3];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(thisTransform.forward, boxLayer);
            nearbyBoxes[2] = CheckDirection(-thisTransform.right, boxLayer);

            //if the box on the right is dirty, go clean
            if (!CheckForBoxStatus(nearbyBoxes[0]))
                StartMovement(nearbyBoxes[0]);

            //if the box ahead is dirty, go clean
            else if (!CheckForBoxStatus(nearbyBoxes[1]))
                StartMovement(nearbyBoxes[1]);

            //if the box on the left is dirty, go clean
            else if (!CheckForBoxStatus(nearbyBoxes[2]))
                StartMovement(nearbyBoxes[2]);
        }
        else if (m_rightWall && m_forwardWall && m_leftWall)
        {
            //there are walls on all the sensors sides, lets rotate to the right
            StartRotation(m_rightWall);
        }
        else if (m_rightWall)
        {
            //there is a wall on the right side
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.forward, boxLayer);
            nearbyBoxes[1] = CheckDirection(-thisTransform.right, boxLayer);

            if (nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if the box ahead is dirty, go clean
                if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the box on the left is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check which one of the boxes is missing, and in case, move to the other existing
                if (nearbyBoxes[0] && !CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                else if (nearbyBoxes[1] && !CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
        }
        else if (m_forwardWall)
        {
            //there is a wall ahead
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(-thisTransform.right, boxLayer);

            if (nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if the right box is dirty, go clean
                if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the left box is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check which one of the boxes is missing, and in case, move to the other existing
                if (nearbyBoxes[0] && !CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                else if (nearbyBoxes[0] && !CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
        }
        else if (m_leftWall)
        {
            //there is a wall on the left
            GameObject[] nearbyBoxes = new GameObject[2];

            nearbyBoxes[0] = CheckDirection(thisTransform.right, boxLayer);
            nearbyBoxes[1] = CheckDirection(thisTransform.forward, boxLayer);

            if (nearbyBoxes[0] && nearbyBoxes[1])
            {
                //if the right box is dirty, go clean
                if (!CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                //if the box ahead is dirty, go clean
                else if (!CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
            else
            {
                //lets check if one of the boxes is missing, and in case, move to the other existing
                if (nearbyBoxes[0] && !CheckForBoxStatus(nearbyBoxes[0]))
                    StartMovement(nearbyBoxes[0]);

                else if (nearbyBoxes[1] && !CheckForBoxStatus(nearbyBoxes[1]))
                    StartMovement(nearbyBoxes[1]);
            }
        }
    }
    #endregion

    #region ACTUATOR RELATED
    void StartMovement(GameObject box)
    {
        mover.StartCoroutine(mover.MoveToBox(thisTransform.position, box, mover.moveTime));
        m_currentTarget = box;
    }

    void StartRotation(GameObject box)
    {
        mover.StartCoroutine(mover.RotateTowardsBox(box));
        m_currentTarget = box;
    }
    #endregion

}
