﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoombaCleaner : MonoBehaviour
{
    public void CleanBox(GameObject box)
    {
        box.GetComponent<BoxCheck>().CleanBox();
    }

    public void CleanBox(BoxCheck box)
    {
        box.CleanBox();
    }
}
